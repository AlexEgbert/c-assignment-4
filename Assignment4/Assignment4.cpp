
#include "pch.h"
#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

//emp = employee
struct emp {
	string first;
	string last;
	int id;
	float rate;
	//pay rate
	float hour;
};


int main()
{
	int num = 0;
	
	cout << "Enter the number of employees before we begin";
	cin >> num;
	int gross;
	int total = 0;

	emp *people =  new emp [num];
	while (--num > -1) {
		//heres where we get our info
		cout << " first name of employee " + num ;
		cin >> people[num].first;
		cout << " last name of employee " + num;
		cin >> people[num].last;
		cout << " Identification number of employee " + num;
		cin >> people[num].id;
		cout << " pay rate (per hour) of employee " + num;
		cin >> people[num].rate;
		cout << " work hours (whole numbers, round as you please) of employee " + num;
		cin >> people[num].hour;

		//some variable adjustment
		gross = people[num].hour * people[num].rate;
		total = total + gross;

		cout << "ID number: " + people[num].id;
		cout << "\nName: " + people[num].first + people[num].last;
		cout << "\ngross pay (rounded): " + gross;
	}
	cout << "The total gross pay is: " + total;
	_getch();
}
